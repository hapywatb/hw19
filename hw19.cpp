﻿

#include <iostream>
using namespace std;


class Animal {
private:
  
protected:

public:
   virtual char voice()
    {
       return 0;

    }
};

class cat: public Animal {
    char voice() override {
        cout << "Meow" <<endl;
        return 0;

        }

    };

    class dog : public Animal{
        char voice() override {
            cout << "Woof" <<endl ;
            return 0;

        }
    };

    class ezh :public Animal {
     
        char voice() override {
            cout << "BOOOO" <<endl;
            return 0;

        }
    };

    int main()
    {
        Animal* zoo[3];

        Animal* c = new cat();
        c->voice();
        Animal* d = new dog();
        d->voice();
        Animal* e = new ezh();
        e->voice();
       

        zoo[0] = c;
        zoo[1] = d;
        zoo[2] = e;

        for (int a = 0; a < 3; a++)
        {
            cout << zoo[a]->voice() <<endl;
        }
    }